/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository.entities;

import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 *
 * @author Junyang
 * 
 */
//TODO Exercise 1.3 Step 1 Please refer tutorial exercise. 
public class Property {
    private int id;
    private String address;
    private int numberOfBedrooms;
    private int size;
    private double price;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getNumberOfBedrooms() {
		return numberOfBedrooms;
	}
	public void setNumberOfBedrooms(int numberOfBedrooms) {
		this.numberOfBedrooms = numberOfBedrooms;
	}
	public BigDecimal getSize() {
		BigDecimal nSize = new BigDecimal(size).setScale(1);
		return nSize;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public String getPrice() {
		BigDecimal nPrice = new BigDecimal(price).setScale(2);
		String price = NumberFormat.getCurrencyInstance().format(nPrice);
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
}
