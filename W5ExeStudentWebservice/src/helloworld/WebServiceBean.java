package helloworld;

import java.io.Serializable;

import javax.faces.bean.SessionScoped;
import javax.inject.Named;

import calculatorManagedBean.MonthlyPaymentCalculator;
import helloworld.client.WebServiceClient;


@Named(value = "webServiceBean")
@SessionScoped
public class WebServiceBean implements  Serializable{
	private String name;
	private MonthlyPaymentCalculator m;
	
	private double principle;
	
	private double interestRate;
	private int numberOfYears;
    private String price;
    
	private WebServiceClient webServiceClient;
	public WebServiceBean(){ 
		}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public void setWebServiceClient() {
		webServiceClient = new WebServiceClient();
		String principle = String.valueOf(getPrinciple());
		String interestRate = String.valueOf(getInterestRate());
		String numberOfYears = String.valueOf(getNumberOfYears());
		webServiceClient.setPostName2(getName(),principle,interestRate,numberOfYears);
	}
	
	public double getPrinciple() {
		return principle;
	}
	public void setPrinciple(double principle) {
		this.principle = principle;
	}
	public double getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}
	public int getNumberOfYears() {
		return numberOfYears;
	}
	public void setNumberOfYears(int numberOfYears) {
		this.numberOfYears = numberOfYears;
	}
}	
