package helloworld;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;

/**
 * Session Bean implementation class NameStorageBean
 */
@Singleton
@LocalBean
public class NameStorageBean {

    private String name="World";
    private double price = 0;

	public double getPrice() {
		return price;
	}



	public void setPrice(double price) {
		this.price = price;
	}

	private double principle =0;
	private double interestRate=0;
	private int numberOfYears=0;

	

	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}
	

	public double getPrinciple() {
		return principle;
	}

	public void setPrinciple(double principle) {
		this.principle = principle;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public int getNumberOfYears() {
		return numberOfYears;
	}

	public void setNumberOfYears(int numberOfYears) {
		this.numberOfYears = numberOfYears;
	}

	
}
