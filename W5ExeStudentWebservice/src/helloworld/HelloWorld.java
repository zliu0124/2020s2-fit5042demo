package helloworld;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import calculatorManagedBean.MonthlyPaymentCalculator;

@Path("greeting")
public class HelloWorld {
 
    @Context
    private UriInfo context;
    @EJB
    private NameStorageBean nameStorage;
    @EJB 
    private MonthlyPaymentCalculator m;

	/**
     * Default constructor. 
     */
    public HelloWorld() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Retrieves representation of an instance of HelloWorld
     * @return an instance of String
     */
    @GET
    @Produces("text/html")
    public String getHtml() {
    	return "<html><body><h1>Hello " + nameStorage.getName() + "!</h1>"
    			+"<body><h1>The price is: "+ nameStorage.getPrice() +"</h1>"+
    			"<h1>principle:"+nameStorage.getPrinciple()+"</h1>"+
    			"<h1>interestRate:"+nameStorage.getInterestRate()+"</h1>"+
    			"<h1>numberOfYears:"+nameStorage.getNumberOfYears()+"</h1>"
    					+ "</html>";
    }

    @POST
    @Consumes("application/x-www-form-urlencoded")
    public void setPostName( @FormParam("name") String content,@FormParam("price") double price,@FormParam("principle") String principle,@FormParam("interestRate") String interestRate,@FormParam("numberOfYears") String numberOfYears) {
    	nameStorage.setName(content);
    	double p = Double.parseDouble(principle);
    	double i = Double.parseDouble(interestRate);
    	int n = Integer.parseInt(numberOfYears);
    	DecimalFormat df = new DecimalFormat( "0.00");
    	double nprice = m.calculate(p, n, i);
    	String nnprice = df.format(nprice);
    	price = Double.parseDouble(nnprice);
    	nameStorage.setPrinciple(p);
    	nameStorage.setInterestRate(i);
    	nameStorage.setNumberOfYears(n);
    	nameStorage.setPrice(price);
    }
    
    @PUT
    @Consumes("text/html")
    public void putHtml(String content) {
    	
   }
    
    
    
}